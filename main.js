function Speaker (options) {
    this.surname = options.surname;
    this.givenname = options.givenname;
    this.email = options.email;
    this.biography = options.biography;
    this.isActive = true;
}

Speaker.prototype.getBiography = function (){
    console.log(`${this.surname}. ${this.givenname}: ${this.biography}`)
}

Speaker.prototype.makeInactive = function (date){
    this.inActiveSince = date;
    this.isActive = false;
}

function KeynoteSpeaker (options) {
    Speaker.call(this, options);
    this.websites = options.websites;
    this.keynoteTopics = options.keynoteTopics;
    this.breakouts = options.breakouts;
}

function WorkshopSpeaker (options) {
    Speaker.call(this, options);
    this.workShopTopics = options.workShopTopics;
}
KeynoteSpeaker.prototype = Object.create(Speaker.prototype);
WorkshopSpeaker.prototype = Object.create(Speaker.prototype);
KeynoteSpeaker.prototype.constructor = KeynoteSpeaker;
WorkshopSpeaker.prototype.constructor = WorkshopSpeaker;


const speaker1 = new Speaker({
    surname: 'Mr',
    givenname: 'James',
    email: 'james@email.com',
    biography: 'goes to school'
})

const keynoteSpeaker1 = new KeynoteSpeaker({
    surname: 'Mr',
    givenname: 'Austin',
    email: 'austin@email.com',
    biography: 'goes to school too',
    websites: ['www.austin.com','www.google.com'],
    keynoteTopics: ['making money', 'giving money to james is good'],
    breakouts: ['hand james cash']
})

const workshopSpeaker1 = new WorkshopSpeaker({
    surname: 'Mr',
    givenname: 'Carl',
    email: 'carl@email.com',
    biography: 'goe to chool a well',
    workshopTopics: ['python for dummies', 'going to the apple tore on tueday'],
})


speaker1.makeInactive('1/2/14');
console.log(speaker1);
console.log(keynoteSpeaker1);
console.log(workshopSpeaker1);
speaker1.getBiography();
keynoteSpeaker1.getBiography();
workshopSpeaker1.getBiography();
